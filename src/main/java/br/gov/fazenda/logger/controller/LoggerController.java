package br.gov.fazenda.logger.controller;

import br.gov.fazenda.logger.service.LoggerService;
import br.gov.fazenda.messaging.model.NfeGenericLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LoggerController {
    @Autowired
    private LoggerService service;

    @KafkaListener(topics = "kaique-biro-2", groupId = "kaiqueraLogger")
    public void listenLog(@Payload NfeGenericLog log) {
        service.saveLog(log);
    }
}
