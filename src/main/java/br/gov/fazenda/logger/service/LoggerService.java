package br.gov.fazenda.logger.service;

import br.gov.fazenda.messaging.model.NfeGenericLog;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Service
public class LoggerService {
    public void saveLog(NfeGenericLog log) {
       final String logString = "["+ log.getTime() +"] "+"["+log.getType()+"] " + "["+log.getMessage()+"]\n";
        try {
            Files.write(Paths.get("/home/ubuntu/logs/logs.txt"), logString.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
